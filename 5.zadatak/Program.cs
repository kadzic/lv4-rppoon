﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class Program
    {
        static void Main(string[] args)
        {

            Video video = new Video("Parni valjak - Zastave");
            Book book = new Book("Lara Croft - Tomb Raider");

            HotItem video_item = new HotItem(video);
            HotItem book_item = new HotItem(book);

            List<IRentable> BooksAndVideos = new List<IRentable>()
            {
                new Video("Friday 13th"),
                new Book("Train on the snow"),
                video_item,
                book
            };

            List<IRentable> flashSale = new List<IRentable>()
            {
                new DiscountedItem(50.0, BooksAndVideos[0]),
                new DiscountedItem(20.0, BooksAndVideos[1]),
                new DiscountedItem(30.0, BooksAndVideos[2]),
                new DiscountedItem(80.0, BooksAndVideos[3])
            };

            RentingConsolePrinter printItems = new RentingConsolePrinter();

            Console.WriteLine("Items: ");
            printItems.DisplayItems(BooksAndVideos);
            Console.Write("\n");
            Console.WriteLine("Items on sale: ");
            printItems.DisplayItems(flashSale);

        }
    }
}
