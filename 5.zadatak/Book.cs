﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class Book : IRentable
    {
        private readonly double BasePrice = 3.99;
        public String BookTitle { get; private set; }

        public Book(String Title)
        {
            this.BookTitle = Title;
        }

        public String Description { get { return this.BookTitle; } }

        public double CalculatePrice()
        {
            return this.BasePrice;
        }
    }
}
