﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class EmailValidator : IEmailValidatorService
    {
        public bool ContainsMonkey(String candidate)
        {
            bool result = candidate.Contains("@");
            return result;
        }

        public bool ContainsComOrHr(String candidate)
        {
            bool result = false;
            if(candidate.EndsWith(".hr") || (candidate.EndsWith(".com"))){
                result = true;
            }
            return result;
        }

        public bool IsValidAddress(String candidate)
        {
            return ContainsComOrHr(candidate) && ContainsMonkey(candidate);
        }

        
    }
}
