﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double ItemDiscount;
        public DiscountedItem(double itemDiscount, IRentable rentable) : base(rentable)
        {
            this.ItemDiscount = itemDiscount;
        }

        public override double CalculatePrice()
        {
            double discountPrice = (ItemDiscount / 100) * base.CalculatePrice();
            return base.CalculatePrice() - discountPrice;
        }

        public override string Description
        {
            get
            {
                return base.Description + string.Format(" now at {0}% off!", ItemDiscount.ToString());
            }
        }
    }
}
