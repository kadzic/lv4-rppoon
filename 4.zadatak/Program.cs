﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class Program
    {
        static void Main(string[] args)
        {

            Video video = new Video("Parni valjak - Zastave");
            Book book = new Book("Lara Croft - Tomb Raider");

            HotItem video_item = new HotItem(video);
            HotItem book_item = new HotItem(book);

            List<IRentable> BooksAndVideos = new List<IRentable>()
            {
                new Video("Friday 13th."),
                new Book("Train on the snow."),
                video_item,
                book_item
            };

            RentingConsolePrinter printItems = new RentingConsolePrinter();

            printItems.DisplayItems(BooksAndVideos);
        }
    }
}
