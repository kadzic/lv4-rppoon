﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class Program
    {
        static void Main(string[] args)
        {
            Analyzer3rdParty party = new Analyzer3rdParty();
            Adapter adapter = new Adapter(party);

            Dataset ListsInAList = new Dataset();
        
            ListsInAList.LoadDataFromCSV("C:\\Users\\Korisnik\\source\\repos\\ConsoleApp22\\ConsoleApp22\\LV4.txt");

            double[] rowAverage = adapter.CalculateAveragePerRow(ListsInAList);
            double[] columnAverage = adapter.CalculateAveragePerColumn(ListsInAList);

            Console.WriteLine("Average - row: ");
            foreach (var item in rowAverage)
            {
                Console.WriteLine(item.ToString());
            }

            Console.Write("\n");

            Console.WriteLine("Average - column: ");
            foreach (var item in columnAverage)
            {
                Console.WriteLine(item.ToString());
            }

            Console.Write("\n");
        }
    }
}
