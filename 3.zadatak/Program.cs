﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> BooksAndVideos = new List<IRentable>()
            {
                new Video("Friday 13th."),
                new Book("Train on the snow."),
            };

            RentingConsolePrinter printItems = new RentingConsolePrinter();

            printItems.DisplayItems(BooksAndVideos);
        }
    }
}
