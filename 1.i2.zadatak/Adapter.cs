﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp22
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;

        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }

        private double[][] ConvertData(Dataset dataset)
        {
            int countRows = dataset.GetData().Count;

            double[][] convertedData = new double[countRows][];

            int columnCount = 0;
            foreach (var data in dataset.GetData())
            {
                columnCount = 0;
                foreach (var item in data)
                {
                    columnCount++;
                }
            }

            int i;
            for (i = 0; i < countRows; i++)
            {
                convertedData[i] = new double[columnCount];
            }

            i = 0;
            int k = 0;
            foreach (var data in dataset.GetData())
            {
                k = 0;
                foreach (var item in data)
                {
                    convertedData[i][k] = item;
                    k++;
                }
                i++;
            }

            return convertedData;

        }

        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }

        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
